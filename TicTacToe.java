import java.util.Scanner;
public class TicTacToe
{
	public static void main(String[] args)
	{
		Square[][] board = createEmptyBoard();
		boolean gameActive = true;
		
		Scanner scan = new Scanner(System.in);
		
		int gameResult = 0; //result to be set if there's any win / tie
		int givenValue = 0; //input value, but declared first
		while(gameActive)
		{
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n"); //linebreak to clean the screen
			
			//player1
			boolean player1turnDone = false;
			System.out.println("Player 1 (X)'s turn. \nHere is the current state of the board.");
			print(board);
			System.out.println("\nEnter a spot on the board that corresponds to the numbers below:");
			System.out.println("0 1 2\n3 4 5\n6 7 8");
			while (!player1turnDone)
			{
				givenValue = scan.nextInt();
				if (SwitchBoardSquare(givenValue, Square.X, board))
				{
					player1turnDone = true;
				}
				else
				{
					System.out.println("Invalid number! Please enter again.");
				}
			}
			
			//check if game done
			if(checkIfWinning(board, Square.X) != 0 || checkIfFull(board))
			{
				gameResult = checkIfWinning(board, Square.X);
				break;
			}
			
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n"); //linebreak to clean the screen
				//player2
				boolean player2turnDone = false;
				System.out.println("Player 2 (O)'s turn. \nHere is the current state of the board.");
				print(board);
				System.out.println("\nEnter a spot on the board that corresponds to the numbers below:");
				System.out.println("0 1 2\n3 4 5\n6 7 8");
				while (!player2turnDone)
				{
					givenValue = scan.nextInt();
					if (SwitchBoardSquare(givenValue, Square.O, board))
					{
						player2turnDone = true;
					}
					else
					{
						System.out.println("Invalid number! Please enter again.");
					}
				}
				
			//check if game done
			if(checkIfWinning(board, Square.O) != 0 || checkIfFull(board))
			{
				gameResult = checkIfWinning(board, Square.O);
				break;
			}
			
		}
		
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"); //linebreak to clean the screen
		String resultString = "";
		if(gameResult == 1)
		{
			resultString = "congrats Player 1!";
		}
		else if(gameResult == -1)
		{
			resultString = "congrats Player 2!";
		}
		else
		{
			resultString = "It's a tie, NOBODY WINS!";
		}
		
		System.out.println("Game done. " + resultString + "\n");
		print(board);
	}
	
	
	public static boolean SwitchBoardSquare(int position, Square player, Square[][] board)
	{
			boolean returnValue = false;
			
			int positionY = position/3;
			int positionX = position - (positionY * 3);
			
			if(position >=0 && position <=8 && board[positionY][positionX] == Square.BLANK)
			{
				board[positionY][positionX] = player;
				returnValue = true;
			}
			return returnValue;
	}
	
	public static Square[][] createEmptyBoard()
	{
		Square[][] newArray = new Square[3][3];
		for(int i=0; i<newArray.length;i++)
		{
			for(int j=0; j<newArray[i].length; j++)
			{
				newArray[i][j] = Square.BLANK;
			}
			
		}
		
		return newArray;
	}
	
	public static void print(Square[][] board)
	{	
		for(int i=0;i < board.length; i++)
		{
			for(int j=0;j < board[i].length; j++)
			{
				System.out.print(board[i][j]);
			}
			System.out.println(" ");
		}
	}
	
	public static boolean checkIfFull(Square[][] board)
	{
		for(int i=0;i<board.length;i++)
		{
			for(int j = 0; j<board[i].length; j++)
			{
				if (board[i][j] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean checkIfWinningHorizontal(Square[][] board, Square player)
	{
		boolean rowIsWin = true;
		
		for(int i=0; i< board.length; i++)
		{
			rowIsWin = true;
			for(int j = 0; j<board[i].length; j++)
			{
				if(board[i][j] != player)
				{
					rowIsWin = false;
				}
			}
			if(rowIsWin)
			{
				return true;
			}
		}

		return false;
	}
	
	
	public static boolean checkIfWinningVertical(Square[][] board, Square player)
	{
		boolean columnIsWin = true;
		
		for(int i=0; i< board.length; i++)
		{
			columnIsWin = true;
			for(int j = 0; j<board[i].length; j++)
			{
				if(board[j][i] != player)
				{
					columnIsWin = false;
				}
			}
			if(columnIsWin)
			{
				return true;
			}
		}

		return false;
	}
	
	public static boolean checkIfWinningDiagonal(Square[][] board, Square player)
	{
		
		//check row 1
		if(board[0][0] == player && board[1][1] == player && board[2][2] == player)
		{
			return true;
		}
		//2
		else if(board[2][2] == player && board[1][1] == player && board[0][0] == player)
		{
			return true;
		}
		else{
			return false;
		}
	}
	
	public static int checkIfWinning(Square[][] board, Square player)
	{
		if(checkIfWinningDiagonal(board, player) || checkIfWinningHorizontal(board, player) || checkIfWinningVertical(board, player))
		{
			if(player == Square.X)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return 0;
		}
	}
	
}

